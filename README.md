# spotify-tools

A collection of tools which work using the spotify API

This is **not** suitable for normal use. This proyect exists to train my 
Python skills **which suck** and focus is on getting things to work over 
code quality.

For now the project includes the tool 'spotify-bg' which gets your favorite 
album/song covers on a wallpaper.

This is under the Creative Commons Zero waiver (See WAIVER) and is now hosted
at my [configs repo](https://codeberg.org/Potosi/configs)

### TODO

(Not in order)

- [ ] Scale images to fill whole wallpaper
- [ ] Do not repeat same icons
- [ ] Add padding option
- [x] Add columns
- [ ] Add rows
- [x] Allow artists instead of only songs
- [ ] Allow filtering of albums, artists or songs
- [x] Make some arguments optional
- [x] Config file instead of arguments
- [ ] Clean up code
- [ ] Allow starting pixel location for pasting images

![](output.png)

Btw, I need help with maths on 'spotify-bg'. Specifically, I want to scale 
the images to the maximum size possible having into account display size 
and the ammount.
